import React from "react";

import {BrowserRouter as Router,Routes,Route} from 'react-router-dom';
import Login from "./componentes/auth/Login";
import NuevaCuenta from "./componentes/auth/NuevaCuenta";
import Proyectos from "./componentes/proyectos/Proyectos";


function App() {
  return (
    <Router>
      <Routes>
        <Route  path="/" element ={<Login/>}/>
        <Route  path="/nueva-cuenta" element ={<NuevaCuenta/>}/>
        <Route  path="/proyectos" element ={<Proyectos/>}/>
      </Routes>
    </Router>
  );
}

export default App;
