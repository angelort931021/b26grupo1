import React from 'react';
import NuevoProyecto from '../proyectos/NuevoProyecto';
import Listado from '../proyectos/listado';


const Sidebar = () =>{
return(
    <aside>

    <h1>
        MERN <span>Task</span>
        
        </h1>
        <NuevoProyecto/>

        {/* Componente del sidebar*/}
    
    <div className="proyectos">
        <h2>Tus Proyectos</h2>

        <Listado/>


    </div>
    </aside>
    

);

};

export default Sidebar;
