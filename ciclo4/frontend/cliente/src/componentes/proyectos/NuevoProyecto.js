import React,{Fragment,useState} from 'react'

const NuevoProyecto=()=>{

    const [proyecto, guardarProyecto] = useState({
        nombre:''
    })

    const {nombre}=proyecto

    const onChangeProyecto=e=>{
        guardarProyecto({
            ...proyecto,
            [e.target.name]:e.target.value

        })
}

const onsubmitProyecto=e=>{
    e.preventDefault();

    //validación del proyecto

    //usar o agregar al state

    //reinicar el form

}

return (
<Fragment>
    <button type ="button" className ="btn btn-block btn.primario" >
    Nuevo Proyecto         
    </button>

    <form
        className="formulario-nuevo-proyecto" onSubmit={onsubmitProyecto}
    
        >
         <input
                type="text"
                className ="input-text"
                placeholder="Nombre del proyecto"
                name="nombre"
                onChange={onChangeProyecto}
                value = {nombre}
                
                />

        <input
            type="submit"
            className="btn btn-primario btn-block"
            value="Agregar Proyecto"
            />   

        </form>

</Fragment>

);

}
export default NuevoProyecto;