import React from "react";
import Proyectos from "./Proyectos";


const Listado =()=>{

    const proyectos=[
        {nombre:"Tienda Virtual"},
        {nombre: "Intranet"},
        {nombre:"Diseño de Sitio Web"},
    ];

    return(
        <ul className="listado-proyectos">
        {proyectos.map((proyecto) =>(

            <proyecto proyecto ={proyecto} />
        ))}
        
        </ul>
            
    );
};

export default Listado;